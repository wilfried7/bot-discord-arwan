require("dotenv").config()
const axios = require("axios")


const arwan_api = process.env.ARWAN_API

const getPizzasClassique = async () => {
    const req_pizza_classique = await axios(`${arwan_api}/`)

    return req_pizza_classique.data
}

const getPizzasComposed = async () => {
    const req_pizza_composed = await axios(`${arwan_api}/composed`)

    return req_pizza_composed.data
}

const getAllPizzas = async () => {
    const pizza_info = {}
    pizza_info["classique"] = await getPizzasClassique()
    pizza_info["composed"] = await getPizzasComposed()

    return pizza_info
}

const getPizzasName  = async () => {
    const pizza_info = await getAllPizzas()
    const pizza_noms = {}

    let index = 0
    for(i in pizza_info){
        for(j in pizza_info[i]){
            pizza_noms[index] = pizza_info[i][j].Nom.replace(" ","")
            index += 1
        }
    }
    return pizza_noms
}

const checkPizzaExist = async (nom_pizza) => {
    const pizza_noms = await getPizzasName()

    let pizza_exist = false

    for(i in pizza_noms){
        if(pizza_noms[i].toLowerCase() === nom_pizza.toLowerCase()) pizza_exist = true
    }

    return pizza_exist
}

const checkPizzaIsComposed  = async (nom_pizza) => {
    const pizzas_info = await getPizzasComposed()
    for(i in pizzas_info){
        const pizza = pizzas_info[i]
        const nom = pizza.Nom.toLowerCase().replace(" ","")
        if(nom == nom_pizza){
            let phrase = pizza.Origine
            if(Array.isArray(pizza.IngredientsMoins)) phrase += " sans " + pizza.IngredientsMoins.join(',')
            if(Array.isArray(pizza.IngredientsPlus)) phrase += " avec supplements " + pizza.IngredientsPlus.join(',')
            return phrase
        }
    }
    return false
}

const checkIngrédients = async (nom_pizza, operation, ingredients) => {
    if(operation === '+') return true
    const pizza_info = await getAllPizzas()
    const liste_ingredients = ingredients.split(',')

    for(index in pizza_info.classique){
        if (pizza_info.classique[index].Nom.toLowerCase() === nom_pizza.toLowerCase()){


            for(ingredient in liste_ingredients){
                if(!pizza_info.classique[index].Ingredients.includes(liste_ingredients[ingredient].toLowerCase())){
                    return liste_ingredients[ingredient]
                }
            }
        }
    }
    console.log("true")
    return true
}

module.exports.getPizzasClassique = getPizzasClassique
module.exports.getPizzasComposed = getPizzasComposed
module.exports.getPizzasName = getPizzasName
module.exports.checkIngrédients = checkIngrédients
module.exports.checkPizzaExist = checkPizzaExist
module.exports.checkPizzaIsComposed = checkPizzaIsComposed

