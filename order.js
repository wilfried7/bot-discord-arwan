let pizzas_order = {}

const addOrder = (pseudo, nom_pizza) => {
    pizzas_order[pseudo] = nom_pizza
}

const checkOrder = (pseudo) => {
    return pizzas_order[pseudo]
}

const showOrder = () => {
    return pizzas_order
}

const cleanOrder = () => {
    pizzas_order = {}
}

const removeOrder = (pseudo) => {
    delete pizzas_order[pseudo]
}
 
module.exports.addOrder = addOrder
module.exports.checkOrder = checkOrder
module.exports.showOrder = showOrder
module.exports.cleanOrder = cleanOrder
module.exports.removeOrder = removeOrder