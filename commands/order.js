const {showOrder} = require('../order')
const Discord = require("discord.js")

module.exports = {
    name:"order",
    execute(message, args){
        const orderMessage = new Discord.MessageEmbed()
        .setTitle("C'est l'heure du co-co-commandage")
        .setDescription("Téléphone: 02 96 46 14 14")
        .setFooter("Demandez les pizzas coupées au besoin !")

        const pizzas = showOrder()
        for(index in pizzas){
            orderMessage.addField(index, pizzas[index])
        }

        message.channel.send(orderMessage)
    }
}