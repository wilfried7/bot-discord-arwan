const {addOrder} = require('../order')
const {checkPizzaExist, checkIngrédients, checkPizzaIsComposed} = require("../pizzas")
require("dotenv").config()

const prefix = process.env.PREFIX

const command_name = "add"

module.exports = {
    name: command_name,
    async execute(message, args){
        if(args.length === 0){
            message.channel.send(`Vous devez entrez au moins le nom de la pizza tout attaché (exemple: biquette, 4saisons, ...)\nVous pouvez aussi ajouter ou enlevé des ingrédients\nExemples:\n${prefix} ${command_name} kevin\n${prefix} ${command_name} 4saisons - salade,origan + viandehachée`)
            return
        }
        if(! await checkPizzaExist(args[0])) message.channel.send("Cette pizza n'existe pas")
        const res_composed = await checkPizzaIsComposed(args[0])
        if(res_composed){
            addOrder(message.member.user.username, res_composed)
            return
        }
        let phrase = args[0] + " "
        if(args.length == 1){
            console.log("coucou")
        }

        if(args.length == 2 || args.length == 4){
            let index = 1
            if(args.length == 4) index = 3
            if(args[index] === "-" || args[index] === '+'){
                message.channel.send("Il vous manque les ingrédients (séparés par des virgules) ")
            }else{
                message.channel.send("[+,-] (ingrédient séparés par des virgules)")
            }
        }

        if(args.length === 3 || args.length === 5){
            
            let res = await checkIngrédients(args[0], args[1], args[2])

            if (res !== true) return message.channel.send(res + " n'est pas un ingrédient de cette pizza")

            if(args[1] === '+'){
                phrase += "avec supplément " + args[2] + " "
            }else{
                console.log("coucou")
                phrase += "sans " + args[2] + " "
            }

            if(args.length === 5){
                res = await checkIngrédients(args[0], args[3], args[4])
                if (res !== true) return message.channel.send(res + " n'est pas un ingrédient de cette pizza")

                if(args[3] === '+'){
                    phrase += "avec supplément " + args[4] + " "
                }else{
                    phrase += "sans " + args[4] + " "
                }
            }
        }

        addOrder(message.member.user.username, phrase)
        message.channel.send("Votre commande a été prise en compte !")
    }
}