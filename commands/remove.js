const {removeOrder} = require("../order")

module.exports = {
    name: "remove",
    execute(message, args){
        removeOrder(message.member.user.username)
        message.channel.send("Votre commande a été supprimée !")
    }
}