const {getPizzasClassique, getPizzasComposed} = require("../pizzas")
const Discord = require("discord.js")
require("dotenv").config()

const arwan_url = process.env.ARWAN_URL


module.exports = {
    name: "list",
    async execute(message, args){

        const pizzaEmbed = new Discord.MessageEmbed()
        .setTitle("Liste des pizzas de ARWAN")
        .setURL(arwan_url)
        .setImage("https://www.ar-rwan-pizza.fr/img/style/logo.png")
        .setDescription("Téléphone: 02 96 46 14 14")
        .setFooter("Demandez les pizzas coupées au besoin !")

        pizzaEmbed.addField("PIZZAS CLASSIQUES", "Les pizzas sans modification")
        
        
        const pizzas_classique = await getPizzasClassique()
        const pizza_composed = await getPizzasComposed()
        
        for(i in pizzas_classique){
            pizzaEmbed.addField(pizzas_classique[i].Nom, "Ingrédients: " + pizzas_classique[i].Ingredients.join(', ')
                 + "\nPrix Taille 26: " + pizzas_classique[i].Prix_26 + "\nPrix Taille 33: " + pizzas_classique[i].Prix_33)
        }

        pizzaEmbed.addField("PIZZAS COMPOSÉES", "Les pizzas que l'on a modifiés")

        for(i in pizza_composed){
            pizzaEmbed.addField(pizza_composed[i].Nom, "Composition: " + pizza_composed[i].Origine + " sans " 
                + pizza_composed[i].IngredientsMoins.join(', ') + " avec supplément " 
                + pizza_composed[i].IngredientsPlus.join(', ') + "\nPrix Taille 26: " + pizza_composed[i].Prix_26
                + "\nPrix Taille 33: " + pizza_composed[i].Prix_33)
        }
        

        message.channel.send(pizzaEmbed)
    }
}