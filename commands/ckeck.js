const {checkOrder} = require("../order")

module.exports = {
    name: "check",
    execute(message, args){
        const order = checkOrder(message.member.user.username)
        if(order){
            message.channel.send("Votre commande est: " + order)
        }else{
            message.channel.send("Vous n'avez commandez aucune pizza !\nPour en commander une, utilisez 'add' ")
        }
       
    }
}