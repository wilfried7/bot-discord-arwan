const {cleanOrder} = require("../order")

module.exports = {
    name: "clean",
    execute(message, args){
        cleanOrder()
        message.channel.send("Les commandes ont été supprimées !")
    }
}