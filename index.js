const Discord = require("discord.js")
const fs = require("fs")
require("dotenv").config()

const token = process.env.TOKEN
const prefix = process.env.PREFIX
const client = new Discord.Client()
client.commands = new Discord.Collection();


const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
	const command = require(`./commands/${file}`);
	client.commands.set(command.name, command);
}


client.on("ready", () => {
    console.log("coucou")
})

client.on("message", (message) => {
    if(!message.content.startsWith(prefix)) return
    if(message.content == `${prefix} ping` ){
        message.channel.send("pong pizza")
    }

	const args = message.content.slice(prefix.length).trim().split(/ +/);
    const commandName = args.shift().toLowerCase();

    const command = client.commands.get(commandName)

    if(!command) return
    
    try {
		command.execute(message, args);
	} catch (error) {
		console.error(error);
		message.reply('Il y a eu une erreur en executant votre commande');
	}

})

client.login(token)