# Bot discord pour Ar'rwan pizza
## Présentation
Ce bot discord servira pour commander des pizzas chez ar rwan à Lannion. Il nous permettra de choisir nos pizzas chacun et en une commande, nous allons afficher toutes les commandes avec le numéro, l'habituelle phrase pour se remémorer de les commander coupées

## Afficher la liste des pizzas

`!arwan list`

Cette commande affiche toutes les pizzas disponibles, que ce soit celles d'arwan ou nos pizzas "customisées" (coucou la kevin)

## Commander une pizza

Pour commander une pizza vous devez mettre le nom de la pizza comme affichée avec `!arwan list`. **Les noms ne doivent pas avoir d'espace !**

**Exemples :**

`!arwan add 4saisons`

`!arwan add tartiflette`

`!arwan add kevin`

Vous pouvez aussi ajouter ou retirer des ingrédients mais **uniquement sur les pizzas classiques, pas sur les pizzas customisées**

**Exemples :**

`!arwan add biquette - lardons + viandehachée`

`!arwan add biquette - lardons`

`!arwan add biquette + viandehachée`

Vous pouvez aussi changer votre commande en faisant encore une fois un `!arwan add` : en effet, la commande d'avant sera supprimé et remplacer par la nouvelle

## Afficher votre commande

`!arwan check`

## Supprimer votre commande

`!arwan remove`

## Afficher la commande entière pour commander

`!arwan order`

## Supprimer les commandes

Une fois votre commande passé, vous pouvez supprimez les commandes de tout le monde avec :

`!arwan clean`



